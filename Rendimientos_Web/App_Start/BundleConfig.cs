﻿using System.Web;
using System.Web.Optimization;

namespace Rendimientos_Web
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-2.1.4.js",
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui.js",
                "~/Scripts/DataTables/media/js/jquery.dataTables.js",
                 "~/Scripts/DataTables/media/js/dataTables.bootstrap.js",
                "~/Scripts/jquery.table2excel.js"));

         
            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(               
                "~/Content/bootstrap.css",
                "~/Content/Site.css",              
                "~/Content/DataTables/media/css/dataTables.jqueryui.css",
              //  "~/Content/Datatables/media/css/jquery.dataTables.css",
                "~/Content/Datatables/media/css/dataTables.bootstrap.css"));

    
            //bundles.Add(new ScriptBundle("~/admin-lte/js").Include(
            // "~/admin-lte/js/app.js",
            // "~/admin-lte/plugins/fastclick/fastclick.js"
            // ));
        }
    }
}
