﻿jQuery(document).ready(function () {
    jQuery("#dataExport").click(function () {
        jQuery("#tableLabelPrint").table2excel({
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: "Rendimientos" //do not include extension
        });
    });

    $("#tableLabelPrint").DataTable();

    var total = 0;
    $("#tableLabelPrint tbody tr").find("td:eq(6)").each(function () {
        valor = $(this).html();
        total += parseInt(valor)
    });
    jQuery("#Total").html(total);
});

function onClick() {
    var srtDate = jQuery("#DateInitial").val();

    var CentroOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();
    var values = { "CentroOperacionID": CentroOperacionID, "Fecha": srtDate }

    
    jQuery.ajax({
        url: "/WeeklyCons/Report",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            jQuery('#tableLabelPrint').html(data);
            var sumLunes = 0;
            var sumMartes = 0;
            var sumMiercoles = 0;
            var sumJueves = 0;
            var sumViernes = 0;
            var sumSabado = 0;
            var sumDomingo = 0;

            $("#tableLabelPrint tbody tr").each(function () {
                lunes = parseInt($(this).find("td").eq(1).html());
                martes = parseInt($(this).find("td").eq(2).html());
                miercoles = parseInt($(this).find("td").eq(3).html());
                jueves = parseInt($(this).find("td").eq(4).html());
                viernes = parseInt($(this).find("td").eq(5).html());
                sabado = parseInt($(this).find("td").eq(6).html());
                domingo = parseInt($(this).find("td").eq(7).html());

                lunes = isNaN(lunes) ? 0 : lunes;
                sumLunes += lunes;
                jQuery("#tLunes").text(sumLunes);

                martes = isNaN(martes) ? 0 : martes;
                sumMartes += martes;
                jQuery("#tMartes").text(sumMartes);

                miercoles = isNaN(miercoles) ? 0 : miercoles;
                sumMiercoles += miercoles;
                jQuery("#tMiercoles").text(sumMiercoles);

                jueves = isNaN(jueves) ? 0 : jueves;
                sumJueves += jueves;
                jQuery("#tJueves").text(sumJueves);

                viernes = isNaN(viernes) ? 0 : viernes;
                sumViernes += viernes;
                jQuery("#tViernes").text(sumViernes);

                sabado = isNaN(sabado) ? 0 : sabado;
                sumSabado += sabado;
                jQuery("#tSabado").text(sumSabado);

                domingo = isNaN(domingo) ? 0 : domingo;
                sumDomingo += domingo;
                jQuery("#tDomingo").text(sumDomingo);

                total = sumLunes + sumMartes + sumMiercoles + sumJueves + sumViernes + sumSabado + sumDomingo;
                jQuery("#ttotal").text(total);

                $(this).find("td").eq(8).text(lunes + martes + miercoles+ jueves + viernes + sabado + domingo);
            });
        },
        error: function () {
        }
    });
}
