﻿jQuery(document).ready(function () {

    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = now.getFullYear() + "-" + (month) + "-" + (day);
    $('#Date').val(today);


    var CentrosOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();

    $('#CollaboratorID').on('input', function (e) {

        if ($(this).val().length > 3) {
            var data = $(this).val();
            var values = { "businessUnitID": CentrosOperacionID, "name": data }

            jQuery.ajax({
                url: "/Change/searchCollaborator",
                data: JSON.stringify(values),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $("#lstCollaborator").empty();
                    for (var i = 0; i < data.length; i++) {
                        $("#lstCollaborator").append("<option data-value='" + data[i].ID + "' value='" + data[i].Nombre + "'>" + data[i].Nombre + "</option>");
                        //   $("#lstPhysicalAddressCity").append("<option id='" + data[i].geonameid + "' value='" + data[i].City + "'>" + data[i].City +"</option>");
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr)
                },
            });
        } 
    });

    $('#lstCentrosOperacion').click(function () {
        var data = "";
             var CentrosOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();

            var values = { "businessUnitID": CentrosOperacionID, "name": data }

            jQuery.ajax({
                url: "/Change/searchCollaborator",
                data: JSON.stringify(values),
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $("#lstCollaborator").empty();
                    for (var i = 0; i < data.length; i++) {
                        $("#lstCollaborator").append("<option data-value='" + data[i].ID + "' value='" + data[i].Nombre + "'>" + data[i].Nombre + "</option>");
                        //   $("#lstPhysicalAddressCity").append("<option id='" + data[i].geonameid + "' value='" + data[i].City + "'>" + data[i].City +"</option>");
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(xhr)
                },
            });
    });
});

function onClickSave() {

    var BusinessUnitID = jQuery("#lstCentrosOperacion").find(":selected").val();
    var Date = jQuery("#Date").val();
    var ReasonID = jQuery("#lstChange").find(":selected").val();
    var CollaboratorID = jQuery("#CollaboratorID").val();
    var HourInitial = jQuery("#HourInitial").val();
    var HourEnd = jQuery("#HourEnd").val();



    var parameters = { "BusinessUnitID": BusinessUnitID, "ReasonID": ReasonID, "CollaboratorID": CollaboratorID, "Date": Date, "HourInitial": HourInitial, "HourEnd": HourEnd }
    
    jQuery.ajax({
        url: "/Change/Create",
        data: JSON.stringify(parameters),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            jQuery('#tableNovelty').html(data);
            UnlockScreen();
        },
        error: function (xhr, textStatus, errorThrown) {
            //UnlockScreen();
            //Notifications("error", errorThrown)
        },
    });
}