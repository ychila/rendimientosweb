﻿jQuery(document).ready(function () {
    jQuery("#dataExport").click(function () {
        jQuery("#tableLabelPrint").table2excel({
            exclude: ".noExl",
            name: "Worksheet Name",
            filename: "Rendimientos" //do not include extension
        });
    });

    $("#tableLabelPrint").DataTable();

    var total = 0;
    $("#tableLabelPrint tbody tr").find("td:eq(6)").each(function () {
        valor = $(this).html();
        total += parseInt(valor)
    });
    jQuery("#Total").html(total);
});

function onClick() {
    var srtDate = jQuery("#DateInitial").val();

    var CentroOperacionID = jQuery("#lstCentrosOperacion").find(":selected").val();
    var values = { "CentroOperacionID": CentroOperacionID, "Fecha": srtDate }

    jQuery.ajax({
        url: "/Summary/Summary",
        data: JSON.stringify(values),
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            jQuery('#tableLabelPrint').html(data);
            var total = 0;
            $("#tableLabelPrint tbody tr").find("td:eq(6)").each(function () {
                valor = $(this).html();
                total += parseInt(valor)
            });
            jQuery("#Total").html(total);
        },
        error: function () {
        }
    });
}
