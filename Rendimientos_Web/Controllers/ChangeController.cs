﻿using Rendimientos_Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rendimientos_Web.Controllers
{
    
    public class ChangeController : Controller
    {
        BASEntities _BASEntities = null;
        // GET: Change
        public ActionResult Index()
        {
            _BASEntities = new BASEntities();

            SelectList lstCentrosOperacion = null;
            var aList = _BASEntities.BusinessUnit.OrderBy(x => x.Name);
            lstCentrosOperacion = new SelectList(aList, "BusinessUnitID", "Name".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            SelectList lstChange = null;
            var aListChange = _BASEntities.PerformanceReason.OrderBy(x => x.Name);
            lstChange = new SelectList(aListChange, "PerformanceReasonID", "Name".Trim().ToUpper());
            ViewData["lstChange"] = lstChange;
            

            var lstCollaborator = _BASEntities.sp_GetCollaborator(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), "").Take(10).OrderBy(x=>x.Nombre).ToList();
            ViewBag.lstCollaborator = lstCollaborator;


            ViewBag.performanceNovelty_List = _BASEntities.sp_GetPerformanceNovelty(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value)).ToList();
            


            return View();
        }

        public ActionResult searchCollaborator(int businessUnitID, string name)
        {
            _BASEntities = new BASEntities();
            
            var lstCollaborator = _BASEntities.sp_GetCollaborator(businessUnitID,name).OrderBy(x=>x.Nombre).ToList();
            ViewBag.lstCollaborator = lstCollaborator;

            return Json(lstCollaborator);
        }

        public ActionResult Create(int BusinessUnitID, int ReasonID, string CollaboratorID, string Date, string HourInitial, string HourEnd)
        {
            try
            {
                _BASEntities = new BASEntities();
                
                var sp = _BASEntities.sp_GetCollaborator(BusinessUnitID, CollaboratorID).Select(x => x.ID).ToList();

                DateTime fecha = Convert.ToDateTime(Date);
                DateTime horaInicial = Convert.ToDateTime(HourInitial);
                DateTime horaFinal = Convert.ToDateTime(HourEnd                                                                 );


                DateTime fechaConvertidaInicial = fecha.AddHours(horaInicial.Hour).AddMinutes(horaInicial.Minute).AddSeconds(horaInicial.Second);
                DateTime fechaConvertidaFinal = fecha.AddHours(horaFinal.Hour).AddMinutes(horaFinal.Minute).AddSeconds(horaFinal.Second);


                var a = Convert.ToInt32(sp[0]); ;
                PerformanceNovelty _performanceNovelty = new PerformanceNovelty();
                _performanceNovelty.BusinessUnitID = BusinessUnitID;
                _performanceNovelty.ReasonID = ReasonID;
                _performanceNovelty.CollaboratorID = Convert.ToInt32(sp[0]);
                _performanceNovelty.Date = Convert.ToDateTime(Date);
                _performanceNovelty.HourInitial = fechaConvertidaInicial;
                _performanceNovelty.HourEnd = fechaConvertidaFinal;

                NoveltyInsert(_performanceNovelty);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            var performanceNovelty_List = _BASEntities.sp_GetPerformanceNovelty(BusinessUnitID).ToList();
            ViewBag.performanceNovelty_List = performanceNovelty_List;

            return PartialView("performanceNovelty_List");

        }

        public void NoveltyInsert(PerformanceNovelty _performanceNovelty)
        {
            try
            {
                _BASEntities.PerformanceNovelty.Add(_performanceNovelty);
                _BASEntities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public ActionResult showReport(int BusinnesUnitID, string Date)
        //{
        //    var performanceNovelty_List = _BASEntities.sp_GetPerformanceNovelty(BusinessUnitID).ToList();
        //    ViewBag.performanceNovelty_List = performanceNovelty_List;
        //}
    }
}