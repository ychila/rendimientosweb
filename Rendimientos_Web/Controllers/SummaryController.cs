﻿using Rendimientos_Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rendimientos_Web.Controllers
{
    
    public class SummaryController : Controller
    {
        BASEntities _BASEntities = null;
        // GET: Summary
        public ActionResult Index()
        {
            _BASEntities = new BASEntities();

            SelectList lstCentrosOperacion = null;
            var aList = _BASEntities.BusinessUnit.OrderBy(x => x.Name);
            lstCentrosOperacion = new SelectList(aList, "BusinessUnitID", "Name".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            Summary(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToDateTime("2019-04-09"));

            return View();
        }

        public ActionResult Summary(int CentroOperacionID, DateTime Fecha)
        {
            _BASEntities = new BASEntities();

            var Summary_List = _BASEntities.sp_GetPerformanceSummary(CentroOperacionID, Fecha).ToList();
            ViewBag.Summary_List = Summary_List;

            return PartialView("Summary_List");
        }
    }
}