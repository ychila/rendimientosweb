﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Rendimientos_Web.Controllers
{
    public class HelperController : Controller
    {
        // GET: Helper
        public static string VerifyExportEnvironment(string strFileName)
        {
            try
            {
                if (!System.IO.Directory.Exists(HostingEnvironment.MapPath("\\Export")))
                {
                    System.IO.Directory.CreateDirectory(HostingEnvironment.MapPath("\\Export"));
                }
                var fileLocation = string.Format("{0}/{1}", HostingEnvironment.MapPath("\\Export"), strFileName);

                if (System.IO.File.Exists(fileLocation))
                {
                    System.IO.File.Delete(fileLocation);
                }
                return fileLocation;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}