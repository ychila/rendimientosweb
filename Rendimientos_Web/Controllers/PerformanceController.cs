﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Rendimientos_Web.Models;

namespace Rendimientos_Web.Controllers
{
    public class PerformanceController : Controller
    {
        BASEntities _BASEntities = null;
        // GET: Performance
        public ActionResult Index()
        {
            _BASEntities = new BASEntities();

            SelectList lstCentrosOperacion = null;
            var aList = _BASEntities.BusinessUnit.OrderBy(x => x.Name);
            lstCentrosOperacion = new SelectList(aList, "BusinessUnitID", "Name".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;


            Performance(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToDateTime("2019-04-21"));
            PerformanceTotal(Convert.ToInt32(lstCentrosOperacion.FirstOrDefault().Value), Convert.ToDateTime("2019-04-21"));
            return View();
        }

        public ActionResult Performance(int CentroOperacionID, DateTime Fecha)
        {
            _BASEntities = new BASEntities();
            

            var Performance_List = _BASEntities.sp_GetPerformance(CentroOperacionID, Fecha).ToList();
            ViewBag.Performance_List = Performance_List;

            return PartialView("Performance_List");
        }

        public ActionResult PerformanceTotal(int CentroOperacionID, DateTime Fecha)
        {
            _BASEntities = new BASEntities();


            var data  = _BASEntities.sp_GetPerformance(6, Fecha).GroupBy(x => x.Proceso).Select(y => new { Proceso = y.Key, Cantidadtallos = y.Sum(s => s.Cantidadtallos) });

            var forecastRates = new List<dynamic>();

            foreach (var fr in data)
            {
                dynamic f = new ExpandoObject();
                f.Proceso = fr.Proceso;
                f.Cantidadtallos = fr.Cantidadtallos;
                forecastRates.Add(f);
            }


            ViewBag.Process_List = forecastRates.ToList();
           
            
            return PartialView("Process_List");
        }
    }
}