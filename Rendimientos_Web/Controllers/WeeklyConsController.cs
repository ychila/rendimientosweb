﻿using Rendimientos_Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Rendimientos_Web.Controllers
{
    public class WeeklyConsController : Controller
    {
        BASEntities _BASEntities = null;
        // GET: WeeklyCons
        public ActionResult Index()
        {
            _BASEntities = new BASEntities();

            SelectList lstCentrosOperacion = null;
            var aList = _BASEntities.BusinessUnit.OrderBy(x => x.Name);
            lstCentrosOperacion = new SelectList(aList, "BusinessUnitID", "Name".Trim().ToUpper());
            ViewData["lstCentrosOperacion"] = lstCentrosOperacion;

            Report(Convert.ToInt32(0), "2019-W20");

            return View();
        }

        public ActionResult Report(int CentroOperacionID, string Fecha)
        {
            _BASEntities = new BASEntities();

            var WeeklyCons_List = _BASEntities.sp_GetPerformanceWeek(CentroOperacionID, Fecha).ToList();
            ViewBag.WeeklyCons_List = WeeklyCons_List;
                
            return PartialView("WeeklyCons_List");
        }

    }
}